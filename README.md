# Example Broker

Small example of usage of the broker framework.

A implementation on top of the Actor Framework to abstract handling messages between actors.

## Getting started

* Clone the project
* Execute Launcher
* Subscribe or unsubscribe using the Actor Core from Subscriber Actor.
* Enable debugging using the project flag "BROKER_DEBUG = True"

